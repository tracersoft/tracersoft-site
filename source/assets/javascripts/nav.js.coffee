$(document).ready ->
  menuToggle = $("#js-mobile-menu").unbind()
  $("#js-navigation-menu").removeClass "show"
  menuToggle.on "click", (e) ->
    $('header.navigation').addClass 'mobile-open'
    e.preventDefault()
    $("#js-navigation-menu").slideToggle ->
      if $("#js-navigation-menu").is(":hidden")
        $('header.navigation').removeClass 'mobile-open'
        $("#js-navigation-menu").removeAttr "style"
